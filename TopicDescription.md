# “SDV-style” Programming Model”

Goal: enable efficient and fast SDV application development by offering “surfaces” (APIs, data points) for interacting with the vehicle in a semantically well-defined way.

[Wiki documentation](https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-technical-alignment/sdv-technical-topics/programming-model-sdv-topic/-/wikis/home) 

# Related SDV projects:
-  [Eclipse Velocitas](https://projects.eclipse.org/projects/automotive.velocitas) 
-  [Eclipse Chariott](https://projects.eclipse.org/projects/automotive.chariott) 
-  [Eclipse Kuksa](https://projects.eclipse.org/projects/automotive.kuksa)
-  [Eclipse Ibeji](https://projects.eclipse.org/projects/automotive.ibeji)
-  [Eclipse SommR](https://projects.eclipse.org/projects/automotive.sommr)

# Use cases:
-   “v1” - collate and unify information flows into SDV space
    -  A fleet management client runs in a container, using rFMS-style data consumed in a pub-sub pattern using VSS datapoint definitions
-  “v2” - API/proxy/adapter infrastructure for interacting with vehicle from SDV applications
    -  “If authorized user request is received OTA, open the trunk!”
- "v3" - 
    - ‘Deal with torque repartition between different energies (fuel, h2, elec), energy recuperation’ - make SDV-style data flows and APIs available to other, non-SDV domains

# Cross Initiatives Collaboration 
- COVESA VSS, VSC (already ongoing)
- Middleware like zenoh.io






